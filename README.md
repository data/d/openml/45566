# OpenML dataset: SantanderCustomerSatisfaction

https://www.openml.org/d/45566

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Banco Santander  
**Source**: Unknown - 3-04-2019  
**Please cite**: Unknown

At Santander our mission is to help people and businesses prosper. We are always looking for ways to help our customers understand their financial health and identify which products and services might help them achieve their monetary goals. Our data science team is continually challenging our machine learning algorithms, working with the global data science community to make sure we can more accurately identify new ways to solve our most common challenge, binary classification problems such as: is a customer satisfied? Will a customer buy this product? Can a customer pay this loan?

**Dataset taken from Kaggle** https://www.kaggle.com/c/santander-customer-transaction-prediction/data

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45566) of an [OpenML dataset](https://www.openml.org/d/45566). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45566/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45566/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45566/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

